package com.csu.mall.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.csu.mall.entity.UserEntity;
import org.springframework.stereotype.Repository;

/**
 * @author jingwenLuo
 * @date 2020/6/9 3:00
 */
@Repository
public interface UserMapper extends BaseMapper<UserEntity> {
}
