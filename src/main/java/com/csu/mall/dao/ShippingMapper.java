package com.csu.mall.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.csu.mall.entity.ShippingEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author lhz
 * @email 194712240@csu.edu.cn
 * @date 2020-07-03 15:47:31
 */
@Mapper
public interface ShippingMapper extends BaseMapper<ShippingEntity> {
	
}
