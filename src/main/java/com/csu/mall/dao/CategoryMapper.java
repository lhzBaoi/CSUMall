package com.csu.mall.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.csu.mall.entity.CategoryEntity;
import org.springframework.stereotype.Repository;

/**
 * @author jingwenLuo
 * @date 2020/6/14 21:14
 */
@Repository
public interface CategoryMapper extends BaseMapper<CategoryEntity> {

}
