package com.csu.mall.dao;

import com.csu.mall.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author liuhongzhe
 * @email 194712240@csu.edu.cn
 * @date 2020-06-08 15:59:53
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderEntity> {
	
}
