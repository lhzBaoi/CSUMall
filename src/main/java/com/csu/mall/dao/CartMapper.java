package com.csu.mall.dao;

import com.csu.mall.entity.CartEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 
 * 
 * @author liuhongzhe
 * @email 194712240@csu.edu.cn
 * @date 2020-06-08 15:59:53
 */
//*采用Repository注解*/
@Repository
public interface CartMapper extends BaseMapper<CartEntity> {
	
}
