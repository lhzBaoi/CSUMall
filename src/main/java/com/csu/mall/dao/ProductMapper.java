package com.csu.mall.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.csu.mall.entity.ProductEntity;
import org.springframework.stereotype.Repository;

/**
 * @author jingwenLuo
 * @date 2020/6/14 21:21
 */
@Repository
public interface ProductMapper extends BaseMapper<ProductEntity> {
}
