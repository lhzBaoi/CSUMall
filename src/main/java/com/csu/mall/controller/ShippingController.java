package com.csu.mall.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


import com.csu.mall.common.Constant;
import com.csu.mall.common.ResponseStatusCode;
import com.csu.mall.common.UniformResponse;
import com.csu.mall.entity.ShippingEntity;
import com.csu.mall.entity.UserEntity;
import com.csu.mall.service.ShippingService;
import com.csu.mall.vo.CartVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;


/**
 * 
 *
 * @author lhz
 * @email 194712240@csu.edu.cn
 * @date 2020-07-03 15:47:31
 */
@RestController
@RequestMapping("/shipping")
public class ShippingController {
    @Autowired
    private ShippingService shippingService;

    @GetMapping("/list")
    public UniformResponse<List<ShippingEntity>> getShippingList(){
        return shippingService.getShippingList();
    }

    @PostMapping("/update")
    public UniformResponse<String> updateShipping(ShippingEntity shippingEntity) {
        return shippingService.updateShipping(shippingEntity);
    }

    @PostMapping("/create")
    public UniformResponse<String> createShipping(ShippingEntity shippingEntity){
        System.out.println(shippingEntity);
        return shippingService.createShipping(shippingEntity);
    }

    @PostMapping("/delete")
    public UniformResponse<String> deleteShipping(Integer shippingId){
        return shippingService.deleteShipping(shippingId);
    }


}
