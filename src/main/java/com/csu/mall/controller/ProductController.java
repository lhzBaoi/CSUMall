package com.csu.mall.controller;

import com.csu.mall.common.Constant;
import com.csu.mall.common.UniformResponse;
import com.csu.mall.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jingwenLuo
 * @date 2020/6/15 7:57
 */
@Controller
@RequestMapping(Constant.productRouting.PRODUCT)
public class ProductController {

    @Autowired
    private ProductService productService;


    /**
     * 查看产品详细信息
     * @param productId
     * @return
     */
    @GetMapping(Constant.productRouting.GET_PRODUCT)
    @ResponseBody
    public Object getProductDetail(Integer productId) {
        UniformResponse<Object> response = productService.getProductDetail(productId);
        return response;
    }

    /**
     * 通过categoryId或keyword搜索相关的产品信息列表
     * @param categoryId
     * @param keyword
     * @param pageNum
     * @param pageSize
     * @param orderBy
     * @return
     */
    @GetMapping(Constant.productRouting.GET_PRODUCT_LIST)
    @ResponseBody
    public Object getProductList(@RequestParam(name = "categoryId", required = false) Integer categoryId,
                                 @RequestParam(name = "keyword", required = false) String keyword,
                                 @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                 @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
                                 @RequestParam(name = "orderBy", defaultValue = "") String orderBy) {
        UniformResponse<Object> response = productService.getProductList(categoryId, keyword, pageNum, pageSize, orderBy);
        return response;
    }





}
