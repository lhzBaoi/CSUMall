package com.csu.mall.controller;


import com.csu.mall.common.Constant;
import com.csu.mall.common.UniformResponse;
import com.csu.mall.vo.OrderPageVo;
import com.csu.mall.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.csu.mall.service.OrderService;


/**
 * 
 *
 * @author liuhongzhe
 * @email 194712240@csu.edu.cn
 * @date 2020-06-08 15:59:53
 */
@RestController
@RequestMapping(Constant.orderRouting.ORDER)
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping(Constant.orderRouting.CREATE_ORDER)
    public UniformResponse<OrderVo> createOrder(Integer shippingId){
        UniformResponse<OrderVo> response = orderService.createOrder(shippingId);
        return response;
    }

    @GetMapping(Constant.orderRouting.CANCEL_ORDER)
    public UniformResponse<String> cancelOrder(Long orderNo){
        UniformResponse<String> response = orderService.cancelOrder(orderNo);
        return response;
    }

    @GetMapping(Constant.orderRouting.CONFIRM_ORDER)
    public UniformResponse<String> ConfirmOrder(Long orderNo){
        UniformResponse<String> response = orderService.confirmOrder(orderNo);
        return response;
    }

    /**
     * 查询订单详情 每个用户只有看自己的订单的权限
     * @param
     * @return
     */
    @GetMapping(Constant.orderRouting.GET_ORDER_DETAIL)
    public UniformResponse<OrderVo> getOrderDetail(Long orderNo){
        UniformResponse<OrderVo> response = orderService.getOrderDetail(orderNo);
        return response;
    }

    @GetMapping(Constant.orderRouting.GET_ORDER_LIST)
    public UniformResponse<OrderPageVo> getOrderList(@RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                                     @RequestParam(name = "pageSize", defaultValue = "10") int pageSize){
        UniformResponse<OrderPageVo> response = orderService.getOrderList(pageNum, pageSize);
        return response;
    }





}
