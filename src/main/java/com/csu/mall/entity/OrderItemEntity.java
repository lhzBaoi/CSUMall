package com.csu.mall.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * 
 * @author liuhongzhe
 * @email 194712240@csu.edu.cn
 * @date 2020-06-08 15:59:53
 */
@Data
@TableName("csumall_order_item")
public class OrderItemEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单商品表ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 订单号
	 */
	private Long orderNo;
	/**
	 * 商品ID
	 */
	private Integer productId;
	/**
	 * 商品名称
	 */
	private String productName;
	/**
	 * 商品图片URL
	 */
	private String productImage;
	/**
	 * 订单中的商品单价
	 */
	private BigDecimal currentPrice;
	/**
	 * 商品数量
	 */
	private Integer quantity;
	/**
	 * 订单商品总价
	 */
	private BigDecimal totalPrice;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;
	/**
	 * 最后修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateTime;

}
