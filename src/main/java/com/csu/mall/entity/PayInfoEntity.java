package com.csu.mall.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * 
 * @author liuhongzhe
 * @email 194712240@csu.edu.cn
 * @date 2020-06-08 15:59:53
 */
@Data
@TableName("csumall_pay_info")
public class PayInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 支付表ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 订单号
	 */
	private Long orderNo;
	/**
	 * 线上支付平台：1-支付宝，2-微信支付
	 */
	private Integer payPlatform;
	/**
	 * 线上平台返回支付流水号
	 */
	private String platformNumber;
	/**
	 * 线上平台返回支付状态或信息
	 */
	private String platformStatus;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;
	/**
	 * 最后修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateTime;

}
