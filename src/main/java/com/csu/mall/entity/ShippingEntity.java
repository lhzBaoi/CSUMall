package com.csu.mall.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * 
 * @author liuhongzhe
 * @email 194712240@csu.edu.cn
 * @date 2020-06-08 15:59:53
 */
@Data
@TableName("csumall_shipping")
public class ShippingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 地址ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 收货人姓名
	 */
	private String addressName;
	/**
	 * 固定电话
	 */
	private String addressPhone;
	/**
	 * 手机号码
	 */
	private String addressMobile;
	/**
	 * 省份
	 */
	private String addressProvince;
	/**
	 * 城市
	 */
	private String addressCity;
	/**
	 * 区县
	 */
	private String addressDistrict;
	/**
	 * 详细地址
	 */
	private String addressDetail;
	/**
	 * 邮编
	 */
	private String addressZip;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;
	/**
	 * 最后修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateTime;

}
