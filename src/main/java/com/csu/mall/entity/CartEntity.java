package com.csu.mall.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * 
 * @author liuhongzhe
 * @email 194712240@csu.edu.cn
 * @date 2020-06-08 15:59:53
 */
@Data
@TableName("csumall_cart")
public class CartEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 购物车ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户ID，用户表主键
	 */
	private Integer userId;
	/**
	 * 商品ID，商品表主键
	 */
	private Integer productId;
	/**
	 * 数量
	 */
	private Integer quantity;
	/**
	 * 是否选择：1-选中，2-未选中
	 */
	private Integer checked;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;
	/**
	 * 最后一次修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateTime;

}
