package com.csu.mall.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * 
 * @author liuhongzhe
 * @email 194712240@csu.edu.cn
 * @date 2020-06-08 15:59:53
 */
@Data
@TableName("csumall_order")
public class OrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 订单号
	 */
	private Long orderNo;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 订单地址ID
	 */
	private Integer shippingId;
	/**
	 * 实际支付价格
	 */
	private BigDecimal paymentPrice;
	/**
	 * 支付类型：1-线上支付，其他扩展
	 */
	private Integer paymentType;
	/**
	 * 运费
	 */
	private Integer postage;
	/**
	 * 订单状态：0-已取消，10-未付款，20-已付款，30-已发货，40-交易成功，50-交易关闭
	 */
	private Integer status;
	/**
	 * 支付时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime paymentTime;
	/**
	 * 发货时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime sendTime;
	/**
	 * 交易完成时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime endTime;
	/**
	 * 交易关闭时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime closeTime;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;
	/**
	 * 最后修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateTime;

}
