package com.csu.mall.service;

import com.csu.mall.common.UniformResponse;
import com.csu.mall.entity.CategoryEntity;

import java.util.List;

/**
 * @author jingwenLuo
 * @date 2020/6/15 0:42
 */
public interface CategoryService {

    /**
     * 获取单个商品类别
     * @param categoryId
     * @return
     */
    UniformResponse<CategoryEntity> getCategory(Integer categoryId);

    /**
     * 获取子类的商品类别
     * @param parentId
     * @return
     */
    UniformResponse<List<CategoryEntity>> getChildrenCategory(Integer parentId);

    /**
     * 获取当前分类id及递归子节点Id
     * @param parentId
     * @return
     */
    UniformResponse<List<Integer>> getAllChildrenCategoryId(Integer parentId);

    /**
     * 新增商品类别
     * @param parentId
     * @param categoryName
     * @return
     */
    UniformResponse<String> addCategory(Integer parentId, String categoryName);

    /**
     * 修改商品类别的名称或父类ID
     *
     * @param categoryId
     * @param newName
     * @return
     */
    UniformResponse<String> updateCategoryName(Integer categoryId, String newParentId, String newName);

    UniformResponse<Object> getCategoryTree(Integer categoryId);
}
