package com.csu.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.csu.mall.common.UniformResponse;
import com.csu.mall.entity.ShippingEntity;

import java.util.List;

public interface ShippingService extends IService<ShippingEntity> {
    UniformResponse<List<ShippingEntity>> getShippingList();
    //修改
    UniformResponse<String> updateShipping(ShippingEntity shippingEntity);
    //新增
    UniformResponse<String> createShipping(ShippingEntity shippingEntity);

    UniformResponse<String> deleteShipping(Integer shippingId);
}
