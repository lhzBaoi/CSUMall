package com.csu.mall.service;

import org.springframework.stereotype.Service;

/**
 * @author jingwenLuo
 * @date 2020/7/7 17:01
 */

public interface PayService {

    boolean setPaid(String orderNo);

}
