package com.csu.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.csu.mall.common.UniformResponse;
import com.csu.mall.entity.OrderEntity;
import com.csu.mall.vo.OrderPageVo;
import com.csu.mall.vo.OrderVo;

/**
 * 
 *
 * @author liuhongzhe
 * @email 194712240@csu.edu.cn
 * @date 2020-06-08 15:59:53
 */
public interface OrderService extends IService<OrderEntity> {

    UniformResponse<OrderPageVo> getOrderList(int current, int size);
    UniformResponse<String> cancelOrder(Long orderNo);
    UniformResponse<String> confirmOrder(Long orderNo);
    UniformResponse<OrderVo> getOrderDetail(Long orderNo);
    UniformResponse<OrderVo> createOrder(Integer shippingId);


}

