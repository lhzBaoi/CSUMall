package com.csu.mall.service;

import com.csu.mall.common.UniformResponse;

/**
 * @author jingwenLuo
 * @date 2020/6/15 0:38
 */
public interface ProductService {

    /**
     * 查看产品详细信息
     * @param productId
     * @return
     */
    UniformResponse<Object> getProductDetail(Integer productId);

    /**
     * 搜索产品信息
     * @param categoryId
     * @param keyword
     * @param pageNum
     * @param pageSize
     * @param orderBy
     * @return
     */
    UniformResponse<Object> getProductList(Integer categoryId, String keyword, int pageNum, int pageSize, String orderBy);
}
