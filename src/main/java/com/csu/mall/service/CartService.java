package com.csu.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.csu.mall.common.UniformResponse;
import com.csu.mall.entity.CartEntity;
import com.csu.mall.vo.CartVo;

import java.util.List;

/**
 * 
 *
 * @author liuhongzhe
 * @email 194712240@csu.edu.cn
 * @date 2020-06-08 15:59:53
 */
public interface CartService extends IService<CartEntity> {

    UniformResponse<CartVo> listCart();


    UniformResponse<CartVo> addCart(Integer userId, Integer productId, Integer count);

    UniformResponse<CartVo> update(Integer productId, Integer count);

    UniformResponse<CartVo> deleteProduct(List<Integer> productIds);

    UniformResponse<CartVo> select(Integer productId);

    UniformResponse<CartVo> unSelect(Integer productId);

    UniformResponse<Integer> getCartProductCount();

    UniformResponse<CartVo> selectAll();

    UniformResponse<CartVo> unSelectAll();


}

