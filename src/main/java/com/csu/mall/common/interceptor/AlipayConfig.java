package com.csu.mall.common.interceptor;

//todo 这部分可以写在properties中
public class AlipayConfig {

          // 作为身份标识的应用ID
        public static String app_id = "2016102600764130";

        // 商户私钥，您的PKCS8格式RSA2私钥
        public static String merchant_private_key  = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCBUwF3osrwsLHVdeJRBJKcoDVawoh7OsqUkzyxFh5eIkkIGUmmivKkxNmaVNgXsp5WUFLYHD+wzIpI/UWRImFIG7SfxSgiedeUP5Ii0maDvQZGS8mEMX1qe+/miU236IGoaSewTiM+G8tKn2ODMf76ey+odY6113ijKHVxeDKDuZ37KqOCuTCCo0P5zrKdZU4rOm5ruHvSjUYVQujsgymvxijBwGx7FtttxU3hwN0EmmcFhg2dBhjbeafAHINjzgXSfbivCiDW0h9liU4Ij6FLL7ogDzMeWCGI81A/hBBf8IKOhp1Fa2h3SD03/qHpyDRrRaZwKX8utiZsSXYdCbQ5AgMBAAECggEBAIBgBXTdBNcoj192RdJm16Li5hPSR+kL00zpW7XuXvbS0exYLeREydkGD3xD/L6JcOP9mD8uy4qVgDA0Onp3xlnuJr7y14Rhrr5a3JX0hg7/9NqTINc4yWaK4PnYEm4dP5gNyngZGKLutD6/OeliF9pNrmFrUN7xpa9Q2kRpLLvUfCivaHJNUihM34IdwYQNUIwi0p5GlzJfCfKqM+65KphZRdJP6nA6DOFQ7Osc+XidhvFEzlWc/zrKaT5ll5S0XIQedGhpqJtPRDNZsRWJTkXqs1M+3GTD4qNC2ghXIUBguKb6IveiwHcAGOZjp8sPOfcTsYgUufvOQ7tyESTP9oECgYEAwcyqH22dlpp7Ed8kGNsc8Z/abUjIQvknjqDzr9miA6H2SQfGTQUqv83w3kEIB1Zm0+rZOMKYFEogLmOz9kRC33cvY+PWSOlhuvOMXu+MVw53ZqsnBKPBPWrJ+vbge56FE6Nfjn3v5wZgzYOWxl8fg4dRvC0YOAyC7HUtiARKxs8CgYEAqtTMwedP96JGIm5KIT6CdN+KcfVhl/K/Ao9t7AmCezwCoLhBJ+WZArW/upEx0/ViCqwRsl9b7c7jN5hJ/diaMVdsFArtQpBqGSIpXfmuJssKM7t6UNdknnRliN4qyKs/5D9yUfOoneyOuXg6eEHJCIHud2//Q0p2eIKrz0gWlncCgYA2Ifb7JGE6ahgqW8v3bWzeSSp4EDZwDqxv+KFY40+P+DOVHqjTZrgUL3AyhLO7aPT8CMN4SVK1iCcJDix8g4CybtY77q4yW45C7DivY/YtcwsDEJergho+D5Avm7oEjbjaTW4pnR/fxjZP8YuyCWwCD1BTYb08GrKPXgPD1H8WnQKBgDm8rm+ixE5H1Dhy2i8eN21U2C/n5VI7wv8UhuxN0oHgKGs1mx/m464cy249AIBI4RgGS/zjaeiYEgeKOwfGm2aty2OCxenHcXSJLT4L0yZdXVdat952zhUX9fEqVX95EaENKJA2L/kgKKTa07gtU8rCT1yix71wf1X/pB3smWXFAoGAW+8COD5NHWmC8Pa4w8Tp5nL45EYmUKmkc1iDZz7v4TiLDUbO+IKt6/WLQpNWdUVTXPIkTwzOpInDtwF4BwnYkuqY4gFrV23ubgQpE/pGSaXpQriXXmb2q4DFV7rBJ6w8lWFi/ULEchEThDSVm7os3kpPZDsk+G8xy7jgAptMhuo=";
        // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/appDaily.htm?tab=info 对应APPID下的支付宝公钥。
        public static String alipay_public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDIgHnOn7LLILlKETd6BFRJ0GqgS2Y3mn1wMQmyh9zEyWlz5p1zrahRahbXAfCfSqshSNfqOmAQzSHRVjCqjsAw1jyqrXaPdKBmr90DIpIxmIyKXv4GGAkPyJ/6FTFY99uhpiq0qadD/uSzQsefWo0aTvP/65zi3eof7TcZ32oWpwIDAQAB";

        // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
        public static String notify_url = "http://101.200.194.48:8081/order/pay_back";

        // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
        public static String return_url = "http://101.200.194.48:8081/order/pay_back";

        // 签名方式
        public static String sign_type = "RSA2";

        // 字符编码格式
        public static String charset = "utf-8";

        // 支付宝网关
        public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
}
