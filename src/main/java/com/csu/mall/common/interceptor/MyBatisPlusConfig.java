package com.csu.mall.common.interceptor;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jingwenLuo
 * @date 2020/6/15 18:17
 */
@Configuration
@MapperScan("com.csu.mall.dao")
public class MyBatisPlusConfig {

    /**
     * Mybatis-plus分页插件拦截器
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
