package com.csu.mall.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author jingwenLuo
 * @date 2020/6/15 8:11
 */
@Data
public class ProductVo {
    private Integer id;
    private Integer categoryId;

    private Integer parentCategoryId;

    private String name;
    private String subtitle;

    private String imageHost;

    private String mainImage;
    private String subImages;
    private String detail;
    private BigDecimal price;
    private Integer stock;
    private Integer status;

    private String createTime;
    private String updateTime;

}
