package com.csu.mall.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author jingwenLuo
 * @date 2020/6/15 18:00
 */
@Data
public class ProductListVo {

    private Integer id;
    private Integer categoryId;
    private String name;
    private String subtitle;
    private String imageHost;
    private String mainImage;
    private Integer status;
    private BigDecimal price;
    private Integer stock;

}
