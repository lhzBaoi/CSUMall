package com.csu.mall.vo;

import com.csu.mall.entity.OrderEntity;
import com.csu.mall.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class OrderVo extends OrderEntity{
//    private Integer shippingId;
//    private BigDecimal paymentPrice;
//    private Integer paymentType;
//    private Integer postage;
//    private Integer status;
    private List<OrderItemEntity> itemEntities;

    private ShippingVo shippingVo;

    private String imageHost;

}
