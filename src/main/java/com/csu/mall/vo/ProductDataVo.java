package com.csu.mall.vo;

import lombok.Data;

import java.util.List;

/**
 * @author jingwenLuo
 * @date 2020/6/15 18:26
 */
@Data
public class ProductDataVo {
    private int pageNum;
    private int pageSize;
    private String orderBy;

    private List<ProductListVo> list;

    private long total;
    private long size;
    private long current;
    private boolean optimizeCountSql;
    private boolean hitCount;
    private boolean searchCount;
    private long pages;
}
