package com.csu.mall;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.csu.mall.dao.ProductMapper;
import com.csu.mall.dao.UserMapper;
import com.csu.mall.entity.ProductEntity;
import com.csu.mall.entity.UserEntity;
import com.csu.mall.service.PayService;
import com.csu.mall.service.UserService;
import com.csu.mall.vo.ProductVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

@SpringBootTest
class MallApplicationTests {

    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private OSSClient ossClient;

    @Autowired
    private PayService payService;

    @Test
    void contextLoads() {
    }

    /**
     * DAO测试
     */
    @Test
    void testUserMapperSelect() throws Exception{
        List<UserEntity> userEntityList = userMapper.selectList(null);
        userEntityList.forEach(System.out::println);

        QueryWrapper<UserEntity> query = new QueryWrapper<>();
        query.eq("username","test");
        UserEntity userEntity = userMapper.selectOne(query);
        System.out.println(userEntity);
    }


    @Test
    void testUpload() throws FileNotFoundException {
//        // Endpoint以杭州为例，其它Region请按实际情况填写。
//        String endpoint = "http://oss-cn-shenzhen.aliyuncs.com";
//// 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
//        String accessKeyId = "LTAI4G47KskVfyPCK79KbRQN";
//        String accessKeySecret = "8RpLvZWLxV7gw3SpajnteaKXbzBaqG";
//
//// 创建OSSClient实例。
//        //通过上面三个信息配置的
//        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

// 上传文件流。
        InputStream inputStream = new FileInputStream("C:\\Users\\Administrator\\Desktop\\1553867911_27479_af6db6e1_1836_4fef_afbe_6c1908f53e75.png");
        ossClient.putObject("csumall", "bug1.jpg", inputStream);

// 关闭OSSClient。
        ossClient.shutdown();
        System.out.println("上传完成");
    }

    @Test
    void setPaidTest() throws Exception{
        boolean a = payService.setPaid("2020070717443803");
        System.out.println(a);
    }



}
