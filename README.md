# CSUMall-商场项目
**该项目为实训课程的项目，由Spring boot、Spring MVC、Mybatis-plus实现。**
## 小组成员
薛金凯 罗景文 刘弘哲 李洋正
## 项目文档
具体的文档内容参考[这里](https://gitee.com/lhzBaoi/CSUMall/wikis "这里")。
## 其他内容
[前端项目地址](https://gitee.com/lhzBaoi/CSUMall-front)